# Parallel and Reconfigurable Architectures for Embedded Systems
# 2013-09 -- 2014-01

This repo is just for archiving purpose, and I don't expect it to be useful to
anyone else.

It's one fairly research type course. I find it difficult to obtain new
knowledge effectively with only abstract expression. In fact, I prefer some
concrete examples and implementations.

* `analysis`: Compare some emerging popular parallel architectures.

* `review`: Review the comparison made by other people taking this course as
well.

* [Reconfigurable computing on languages and applications](https://docs.google.com/presentation/d/16mhxTR5X_aB9jZgaQlhpm2IWoP4Tbn888ht10wc4R4U/edit?usp=sharing)

